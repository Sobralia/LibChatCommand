# Description

This mod is library that helps you to create commands and subcommand for your mods.
- V0.1.0
- License: GPLv3
- Minetest forum topic is [here](https://forum.minetest.net/viewtopic.php?f=9&t=14897)

For more informations go to the [Wiki](https://framagit.org/Sobralia/LibChatCommand/wikis/home)

# Installation
## Git
```
cd /PATH/TO/DIR/MODS/
git clone git@framagit.org:Sobralia/LibChatCommand.git lib_chatcommand
```
## Download archive
Every versions of this mod are available on the [downloads](https://framagit.org/Sobralia/LibChatCommand/wikis/downloads) section of the wiki
# How to use
Go to [code sample](https://framagit.org/Sobralia/LibChatCommand/wikis/sample-code) section of the wiki

# Planned features

- Help command (actually does not display help information)
- Named params