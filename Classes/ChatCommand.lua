--[[
  LibChatCommand
  Commands and subcommands library for Minetest modding
 
  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

local Command = dofile(minetest.get_modpath("lib_chatcommand") .. "/Classes/Command.lua")
local permissions_lib_path = minetest.get_modpath("lib_permissions")

ChatCommand = {}
ChatCommand.commands = {}

ChatCommand.__index = ChatCommand
setmetatable(ChatCommand, {__index = Command})

function ChatCommand.new(name, params)
	local privs = params.privs
	local description = params.description
	local func = params.func
	local local_params = params.params
  local self = Command.new(name, params)
  local inherit = true
  
  if (params.inherit ~= nil) then
    inherit = params.inherit
  end
  self.inherit = inherit

  --  If lib_permissions is available, do not pass permissions to minetest_register_chat_command
  if permissions_lib_path ~= nil then
    privs = {}
  end
  
  minetest.register_chatcommand(name, {privs, params, description, func = function(name, params)
    return self:exec(name, params)
  end})
  -- add command to the list
  table.insert(ChatCommand.commands, self)
	return self
end
