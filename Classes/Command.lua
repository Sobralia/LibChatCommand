--[[
  LibChatCommand
  Commands and subcommands library for Minetest modding
 
  Copyright (C) 2016 Olivier Cosquer olivier.cosquer@gmail.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

local permissions_lib_path = minetest.get_modpath("lib_permissions")

if permissions_lib_path == nil then
	Permissions = {}
	function Permissions.has_permissions(player_name, privs)
		local players_privs = minetest.get_players_privs(player_name)
		local has_permissions = true

		if players_privs ~= nil then
			local i = 1

			for k,v in pairs(privs) do
				if players_privs[k] == false then
					has_permissions = false
				end
			end
		else
			has_permissions = false
		end
		return has_permissions
	end
end

local Command = {}

Command.__index = Command

function Command.new(name, params)
	local privs = params.privs
	local description = params.description
	local cmd_func = params.func
	local local_params = params.params
	local inherit = true
	local self = setmetatable({}, Command)

	self.name = name
	self.privs = privs
	self.params = local_params
	self.description = description
	self.cmd_func = cmd_func
	self.sub_commands = {}

	if (params.inherit ~= nil) then
		inherit = params.inherit
	end
	self.inherit = inherit
	return self
end

--  name: "foo.bar.foo2.bar2.<...>"
function Command:register_sub_command(name, params)
	local command = nil

	command = Command.new(name, params)

	table.insert(self.sub_commands, command)
	return command
end

function Command:exec(name, params)
	local cmd_func = self.cmd_func
	local has_sub_cmd = true
	local args = self.parse_args(params)
	local sub_command = self
	local has_permissions = true

	has_permissions = self:check_privileges(name)
	while #args and has_sub_cmd == true do
		sub_command = sub_command:get_sub_command(args[1])

		if sub_command ~= nil then
			cmd_func = sub_command.cmd_func
			table.remove(args, 1)
			if self.inherit == false then
				has_permissions = true
			end
			has_permissions = sub_command:check_privileges(name)
		else
			has_sub_cmd = false
		end
	end

	if has_permissions == false then
		return false, "Missing privileges"
	end
	return cmd_func(name, args)
end

function Command:check_privileges(player_name)
	local str_privs = minetest.privs_to_string(self.privs)
	local has_privs = true
	local privs = {}
	local i = 1

	if string.find(str_privs, ",") ~= nil then
		privs = string.split(str_privs, ",")
	elseif #str_privs > 0 then
		privs = {str_privs}
	end
	has_privs = Permissions.has_permissions(player_name, privs)
	return has_privs
end

function Command.parse_args(param)
	local args = {}
	
	for arg in string.gmatch(param, "[^%s]+") do
		table.insert(args, arg)
	end
	return args
end

function Command:get_sub_command(command)
	local sub_commands = self.sub_commands
	local sub_command = nil
	local i = 1

	while i<= #sub_commands and sub_command == nil do
		if command == self.sub_commands[i].name then
			sub_command = sub_commands[i]
		end
		i = i + 1
	end
	return sub_command
end

return Command
